PROJECT = sugarish
THEMES_DIR = $(INSTALL_PREFIX)/share/themes/$(PROJECT)
INSTALL_PREFIX = /usr

INSTALL = install

toolkits = gtk-2.0 gtk-3.0 xfwm4
sources := $(wildcard themes/xfwm4/*.svg)
targets = $(patsubst %.svg,%.xpm,$(sources))
targets_gtk-2.0 = gtkrc
targets_gtk-3.0 = gtk-widgets.css gtk.css settings.ini
targets_gtk-3.0/assets = *.svg
targets_xfwm4 = *.xpm

all: $(targets)

$(targets): %.xpm: %.pgm %.alpha
	ppmtoxpm -alphamask="$*.alpha" "$*.pgm" > "$@"

$(targets:.xpm=.png): %.png: %.svg
	rsvg-convert -w 28 "$<" > "$@"

$(targets:.xpm=.pgm): %.pgm: %.png
	pngtopnm "$<" > "$@"

$(targets:.xpm=.alpha): %.alpha: %.png
	pngtopnm -alpha "$<" > "$@"

install: $(toolkits:%=install_%) install_gtk-3.0/assets
install_basedir:
	$(INSTALL) -m 755 -d $(DESTDIR)$(THEMES_DIR)
$(toolkits:%=install_%) install_gtk-3.0/assets: install_%: install_basedir
	$(INSTALL) -m 755 -d $(DESTDIR)$(THEMES_DIR)/$*
	$(INSTALL) -m 644 -t $(DESTDIR)$(THEMES_DIR)/$* \
		$(targets_$*:%=themes/$*/%)
install_gtk-3.0/assets: install_gtk-3.0

clean:
	rm -f $(foreach x,$(basename $(targets)),$x.xpm $x.alpha $x.pgm $x.png)

.PHONY: all install clean
